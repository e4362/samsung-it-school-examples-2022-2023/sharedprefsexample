package ru.dolbak.notepad;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class MainActivity extends AppCompatActivity {

    EditText editText;
    SharedPreferences notes;
    ListView listView;
    int counter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = findViewById(R.id.editTextTextPersonName);
        listView = findViewById(R.id.listview);

        notes = PreferenceManager.getDefaultSharedPreferences(this);
        counter = notes.getInt("counter", 0);

        readData();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                SharedPreferences.Editor edit = notes.edit();
                edit.remove("note" + position);
                edit.apply();
                readData();
            }
        });
    }

    public void onClick(View view) {
        String text = editText.getText().toString();
        if (text.length() == 0){
            return;
        }
        SharedPreferences.Editor edit = notes.edit();
        edit.putString("note" + counter, text);
        counter++;
        edit.putInt("counter", counter);
        edit.apply();

        readData();



    }

    void readData(){
        String[] texts = new String[counter];
        for (int i = 0; i < counter; i++){
            texts[i] = notes.getString("note" + i, "");
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, texts);
        listView.setAdapter(adapter);
    }
}